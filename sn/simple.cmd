command,\langsummary ,0,0,
command,\langsolforch ,0,0,
command,\langscansch ,0,0,
command,\langpage ,0,0,
command,\langprob ,0,0,
command,\langselfcheck ,0,0,
command,\langdq ,0,0,
command,\langdqs ,0,0,
command,\langnotation ,0,0,
command,\langothernotation ,0,0,
command,\langselvocab ,0,0,
command,\langexploring ,0,0,
command,\langproblems ,0,0,
command,\langkey ,0,0,
command,\langexample ,0,0,
command,\figprefix ,0,0,
command,\normalparafmt ,0,0,
command,\parafmt ,0,0,
command,\normallayout ,0,0,
command,\nomarginlayout ,0,0,
environment,ifrightifleft,2,0,
environment,minipagefullpagewidth,0,0,
command,\topfraction ,0,0,
command,\textfraction ,0,0,
command,\floatpagefraction ,0,0,
command,\mypart ,1,0,
command,\zapcounters ,0,0,
command,\mychaptercmdsa ,0,0,
command,\mychaptercmdsb ,0,0,
command,\mychapter ,3,0,
command,\mychapterwithopener ,3,0,
command,\mychapterwithopenersidecaption ,3,0,
command,\mychapterwithopenersidecaptionanon ,3,0,
command,\mychapterwithnarrowopenerwidecaption ,3,0,
command,\specialchapterwithopener ,4,0,
command,\mychapterwithfullpagewidthopener ,3,0,
command,\mychapterwithfullpagewidthopenernocaption ,2,0,
command,\mychapterwithopenernocaption ,2,0,
command,\hwcaptions ,0,0,
command,\normalcaptions ,0,0,
command,\marg ,1,0,
command,\indentedcorrend ,0,0,
command,\indentedcorrstart ,0,0,
command,\margdown ,2,0,
command,\formatlikecaption ,1,0,
command,\docaptionnorefstep ,1,1,l
command,\docaption ,1,0,
command,\fullpagewidthfig ,2,1,\figprefix \chapdir /figs
command,\fullpagewidthfignocaption ,1,1,\figprefix \chapdir /figs
command,\fullpagewidthfigx ,5,0,
command,\recordpos ,2,0,
environment,margin,3,0,
command,\fig ,4,0,
command,\anonymousfig ,3,0,
command,\figx ,5,0,
command,\trfig ,2,1,
command,\starttextfig ,1,0,
command,\finishtextfig ,2,0,
command,\starttextbox ,1,0,
command,\textboxbody ,2,0,
command,\finishtextbox ,1,0,
command,\figref ,1,0,
command,\subfigref ,2,0,
command,\fignocaption ,2,0,
command,\fignoresize ,2,0,
command,\widefig ,5,1,h
command,\widefigspecial ,2,1,\figprefix \chapdir /figs
command,\widefignocaptionnofloat ,1,1,\figprefix \chapdir /figs
command,\inlinefignocaption ,1,0,
command,\inlinefignocaptionnoresize ,1,0,
command,\widefigsidecaption ,8,0,
command,\narrowfigwidecaptionnofloat ,3,1,t
command,\narrowfigwidecaptionanon ,3,1,t
command,\anonymousinlinefig ,1,0,
command,\figureintoc ,1,0,
command,\figureintocnoresize ,1,0,
command,\figureintocscootx ,2,0,
command,\figureintocscooty ,2,0,
command,\spacebetweenfigs ,0,0,
command,\smspacebetweenfigs ,0,0,
command,\spaceabovewidefig ,0,0,
command,\myindented ,2,0,
command,\egrule ,1,0,
command,\egvrule ,0,0,
command,\egbothrules ,1,0,
command,\egtit ,2,0,
command,\baretit ,1,0,
environment,eg,1,1,3
environment,egwide,1,1,3
command,\xmark ,0,0,
environment,egnoheader,0,0,
command,\egquestion ,0,0,
command,\eganswer ,0,0,
environment,likeeg,0,0,
command,\optionaltopictit ,1,0,
command,\thescctr ,0,0,
command,\scansref ,1,0,
command,\whichsc ,0,0,
environment,selfcheck,1,0,
command,\worked ,2,0,
environment,margtopic,1,0,
environment,optionaltopic,1,1,3
environment,offsettopic,0,1,3
environment,longquote,0,0,
environment,dialogline,1,0,
command,\labelimportantintext ,1,0,
environment,important,0,1,
environment,lessimportant,0,1,
environment,indentedblock,0,0,
command,\der ,0,0,
command,\divg ,0,0,
command,\curl ,0,0,
command,\grad ,0,0,
command,\myvec ,1,0,
command,\myunit ,1,0,
command,\kgunit ,0,0,
command,\gunit ,0,0,
command,\munit ,0,0,
command,\sunit ,0,0,
command,\degunit ,0,0,
command,\degcunit ,0,0,
command,\junit ,0,0,
command,\nunit ,0,0,
command,\kunit ,0,0,
command,\unitdot ,0,0,
command,\momunit ,0,0,
command,\btheta ,0,0,
command,\bomega ,0,0,
command,\balpha ,0,0,
command,\bsigma ,0,0,
command,\btau ,0,0,
command,\bell ,0,0,
command,\gravunit ,0,0,
command,\vc ,1,0,
command,\massenergy ,0,0,
command,\interval ,0,0,
command,\zs ,1,0,
command,\zu ,1,0,
command,\zb ,1,0,
environment,longnoteafterequation,0,0,
command,\mygamma ,0,0,
command,\sfdefault ,0,0,
command,\DeclareMathOperator ,0,0,
command,\AmSfont ,0,0,
command,\,,0,0,
command,\!,0,0,
command,\:,0,0,
command,\negmedspace ,0,0,
command,\;,0,0,
command,\negthickspace ,0,0,
command,\mspace ,1,0,
command,\over ,0,0,
command,\atop ,0,0,
command,\above ,0,0,
command,\overwithdelims ,0,0,
command,\atopwithdelims ,0,0,
command,\abovewithdelims ,0,0,
command,\dfrac ,0,0,
command,\tfrac ,0,0,
command,\dbinom ,0,0,
command,\tbinom ,0,0,
command,\leftroot ,0,0,
command,\uproot ,0,0,
command,\root ,0,0,
command,\boxed ,1,0,
command,\implies ,0,0,
command,\impliedby ,0,0,
command,\nobreakdash ,0,0,
command,\colon ,0,0,
command,\dotsi ,0,0,
command,\longrightarrow ,0,0,
command,\Longrightarrow ,0,0,
command,\longleftarrow ,0,0,
command,\Longleftarrow ,0,0,
command,\longleftrightarrow ,0,0,
command,\Longleftrightarrow ,0,0,
command,\mapsto ,0,0,
command,\longmapsto ,0,0,
command,\hookrightarrow ,0,0,
command,\hookleftarrow ,0,0,
command,\iff ,0,0,
command,\doteq ,0,0,
command,\int ,0,0,
command,\oint ,0,0,
command,\iint ,0,0,
command,\iiint ,0,0,
command,\iiiint ,0,0,
command,\idotsint ,0,0,
command,\MultiIntegral ,1,0,
command,\big ,0,0,
command,\Big ,0,0,
command,\bigg ,0,0,
command,\Bigg ,0,0,
command,\dddot ,1,0,
command,\ddddot ,1,0,
command,\acc@check ,0,0,
command,\acc@error ,0,0,
command,\bmod ,0,0,
command,\pod ,1,0,
command,\pmod ,1,0,
command,\mod ,1,0,
command,\cfrac ,2,1,c
command,\overset ,2,0,
command,\underset ,2,0,
command,\sideset ,3,0,
command,\smash ,0,1,tb
command,\overrightarrow ,0,0,
command,\overleftarrow ,0,0,
command,\overleftrightarrow ,0,0,
command,\underrightarrow ,0,0,
command,\underleftarrow ,0,0,
command,\underleftrightarrow ,0,0,
command,\xrightarrow ,1,1,
command,\xleftarrow ,1,1,
environment,subarray,1,0,
command,\substack ,1,0,
environment,smallmatrix,0,0,
environment,matrix,0,0,
environment,pmatrix,0,0,
environment,bmatrix,0,0,
environment,Bmatrix,0,0,
environment,vmatrix,0,0,
environment,Vmatrix,0,0,
command,\hdotsfor ,1,0,
environment,cases,0,0,
environment,subequations,0,0,
command,\numberwithin ,2,1,\arabic 
command,\eqref ,1,0,
command,\allowdisplaybreaks ,0,1,4
command,\displaybreak ,0,0,
command,\intertext ,0,0,
command,\thetag ,0,0,
command,\raisetag ,1,0,
command,\notag ,0,0,
command,\nonumber ,0,0,
command,\mintagsep ,0,0,
command,\minalignsep ,0,0,
command,\start@aligned ,2,0,
environment,aligned,0,0,
command,\aligned@a ,0,1,c
environment,alignedat,0,0,
command,\alignedat@a ,0,1,c
environment,gathered,0,1,c
environment,gather,0,0,
environment,gather*,0,0,
environment,alignat,0,0,
environment,alignat*,0,0,
environment,xalignat,0,0,
environment,xalignat*,0,0,
environment,xxalignat,0,0,
environment,align,0,0,
environment,align*,0,0,
environment,flalign,0,0,
environment,flalign*,0,0,
environment,split,0,0,
environment,multline,0,0,
environment,multline*,0,0,
environment,equation,0,0,
environment,equation*,0,0,
command,\@EveryShipout@Hook ,0,0,
command,\@EveryShipout@AtNextHook ,0,0,
command,\EveryShipout *,1,0,
command,\AtNextShipout *,1,0,
command,\@EveryShipout@Shipout ,0,0,
command,\@EveryShipout@Test ,0,0,
command,\@EveryShipout@Output ,0,0,
command,\@EveryShipout@Org@Shipout ,0,0,
command,\@EveryShipout@Init *,0,0,
command,\TPMargin ,0,0,
command,\TPMargin@inner ,1,0,
command,\TPMargin@outer ,1,0,
command,\inputprotcode ,0,0,
command,\formatfigref ,1,0,
command,\lmserifmath ,0,0,
command,\showsubsecnum ,1,0,
command,\formatlikechapter ,1,0,
command,\formatlikesection ,1,0,
command,\formatlikesubsection ,1,0,
command,\formatlikesubsubsection ,1,0,
command,\summarych ,2,0,
command,\sumem ,1,0,
command,\hwanshdr ,1,0,
command,\hwsolnhdr ,1,0,
command,\scanshdr ,1,0,
command,\yesiwantarabic ,0,0,
command,\headrulewidth ,0,0,
command,\footrulewidth ,0,0,
command,\printthesection ,0,0,
command,\chaptermark ,1,0,
command,\sectionmark ,1,0,
command,\myheadertext ,0,0,
command,\epigraphlong ,2,0,
command,\epigraph ,2,0,
command,\mysection ,1,1,4
command,\myoptionalsection ,1,1,4
command,\mysubsection ,1,1,4
command,\myoptionalsubsection ,1,1,3
command,\hwremark ,1,0,
command,\photocredit ,1,0,
command,\mythmhdr ,1,0,
command,\thefigctr ,0,0,
command,\tagform@ ,1,0,
command,\theequation ,0,0,
command,\backofchapterboilerplate ,1,0,
command,\dqheaderformat ,1,0,
command,\startdq ,0,0,
command,\startdqs ,0,0,
command,\startdqswithintro ,1,0,
environment,dq,0,0,
command,\thedqctr ,0,0,
command,\hwdifficulty ,0,0,
environment,hwsection,0,1,Problems
command,\currenthwlabel ,0,0,
command,\calccrud ,0,0,
environment,homework,3,0,
environment,homeworkforcelabel,4,0,
command,\hwendpart ,0,1,3
command,\hwhint ,1,0,
command,\hwans ,1,0,
command,\hwsoln ,0,0,
command,\answercheck ,0,0,
command,\displayhwdifficulty ,1,0,
command,\hwtriangle ,0,0,
command,\hwcheckmark ,0,0,
command,\hwflushtrailingstuff ,0,1,3
command,\hwcleartrailingstuff ,0,0,
command,\hwaddtrailingstuff ,1,0,
command,\hwfill ,1,0,
command,\scans ,1,0,
environment,exsection,0,0,
command,\extitle ,2,0,
command,\mynormaltype ,0,0,
command,\myeqnspacing ,0,0,
command,\link ,2,0,
command,\brieftocentry ,2,1,\quad 
command,\brieftocentrywithlink ,2,1,\quad 
command,\toclinewithlink ,4,0,
command,\toclinewithoutlink ,4,0,
command,\formatchtoc ,3,0,
command,\DeclareGraphicsExtensions ,1,0,
